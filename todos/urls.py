from django.urls import path
from todos.views import (
    show_todolist,
    todo_details,
    create_todolist,
    update_todolist,
    delete_todolist,
    create_todoitem,
    update_todoitem,
)

urlpatterns = [
    path("", show_todolist, name="todo_list_list"),
    path("<int:id>/", todo_details, name="todo_list_detail"),
    path("create/", create_todolist, name="todo_list_create"),
    path("<int:id>/edit/", update_todolist, name="todo_list_update"),
    path("<int:id>/delete/", delete_todolist, name="todo_list_delete"),
    path("items/create/", create_todoitem, name="todo_item_create"),
    path("items/<int:id>/edit/", update_todoitem, name="todo_item_update"),
]
