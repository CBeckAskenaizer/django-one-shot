from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


# Create your views here.
def show_todolist(request):
    list = TodoList.objects.all()
    context = {
        "todo_list": list,
    }
    return render(request, "todos/list.html", context)


def todo_details(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {"todo_object": detail}
    return render(request, "todos/detail.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            created_list = form.save()
            return redirect("todo_list_detail", id=created_list.id)

    else:
        form = TodoForm()

        context = {"form": form}

        return render(request, "todos/create.html", context)


def update_todolist(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=post)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            form.save()
            return redirect("todo_list_detail", id=id)

    else:
        form = TodoForm(instance=post)

        context = {"form": form}

        return render(request, "todos/edit.html", context)


def delete_todolist(request, id):
    post = TodoList.objects.get(id=id)
    if request.method == "POST":
        post.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

        context = {"form": form}

        return render(request, "todos/items/create.html", context)


def update_todoitem(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)

    else:
        form = TodoItemForm(instance=post)

        context = {"form": form}

        return render(request, "todos/items/edit.html", context)
